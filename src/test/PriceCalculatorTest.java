import domain.Movie;
import domain.MovieScreening;
import domain.MovieTicket;
import domain.Order;
import org.junit.Test;

import java.time.DayOfWeek;
import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class PriceCalculatorTest {

    @Test
    public void StudentOrderTest() {

        // Movie and Screening
        Movie movie = new Movie("The A Team");
        MovieScreening movieScreening = new MovieScreening(movie, LocalDateTime.now(), 10);
        movie.addScreening(movieScreening);


        // Tickets for a student order
        MovieTicket S_premiumTicketOne = new MovieTicket(movieScreening, false,  1, 1);
        MovieTicket S_premiumTicketTwo = new MovieTicket(movieScreening, false, 1, 2);
        MovieTicket S_normalTicketOne = new MovieTicket(movieScreening, false,  2, 1);
        MovieTicket S_normalTicketTwo = new MovieTicket(movieScreening, false,  2, 2);
        MovieTicket S_normalTicketThree = new MovieTicket(movieScreening, false,  2, 3);
        MovieTicket S_normalTicketFour = new MovieTicket(movieScreening, false, 2, 4);

        // Student Order
        Order studentOrder = new Order(1, true, movieScreening);
        studentOrder.addSeatReservation(S_premiumTicketOne);
        studentOrder.addSeatReservation(S_premiumTicketTwo);
        studentOrder.addSeatReservation(S_normalTicketOne);
        studentOrder.addSeatReservation(S_normalTicketTwo);
        studentOrder.addSeatReservation(S_normalTicketThree);
        studentOrder.addSeatReservation(S_normalTicketFour);

        assertEquals(30.0, studentOrder.calculatePrice(),1e-16);
    }

    @Test
    public void StudentOrderAndPremiumTicketTest(){
        // Movie and Screening
        Movie movie = new Movie("The A Team");
        MovieScreening movieScreening = new MovieScreening(movie, LocalDateTime.now(), 10);
        movie.addScreening(movieScreening);


        // Tickets for a student order
        MovieTicket S_premiumTicketOne = new MovieTicket(movieScreening, true,  1, 1);
        MovieTicket S_premiumTicketTwo = new MovieTicket(movieScreening, true, 1, 2);
        MovieTicket S_normalTicketOne = new MovieTicket(movieScreening, true,  2, 1);
        MovieTicket S_normalTicketTwo = new MovieTicket(movieScreening, true,  2, 2);
        MovieTicket S_normalTicketThree = new MovieTicket(movieScreening, true,  2, 3);
        MovieTicket S_normalTicketFour = new MovieTicket(movieScreening, true, 2, 4);

        // Student Order
        Order studentOrder = new Order(1, true, movieScreening);
        studentOrder.addSeatReservation(S_premiumTicketOne);
        studentOrder.addSeatReservation(S_premiumTicketTwo);
        studentOrder.addSeatReservation(S_normalTicketOne);
        studentOrder.addSeatReservation(S_normalTicketTwo);
        studentOrder.addSeatReservation(S_normalTicketThree);
        studentOrder.addSeatReservation(S_normalTicketFour);

        assertEquals(42.0, studentOrder.calculatePrice(),1e-15);
    }
    @Test
    public void MixedStudentOrder(){
        // Movie and Screening
        Movie movie = new Movie("The A Team");
        MovieScreening movieScreening = new MovieScreening(movie, LocalDateTime.now(), 10);
        movie.addScreening(movieScreening);


        // Tickets for a student order
        MovieTicket S_premiumTicketOne = new MovieTicket(movieScreening, true,  1, 1);
        MovieTicket S_premiumTicketTwo = new MovieTicket(movieScreening, true, 1, 2);
        MovieTicket S_normalTicketOne = new MovieTicket(movieScreening, false,  2, 1);
        MovieTicket S_normalTicketTwo = new MovieTicket(movieScreening, false,  2, 2);
        MovieTicket S_normalTicketThree = new MovieTicket(movieScreening, true,  2, 3);
        MovieTicket S_normalTicketFour = new MovieTicket(movieScreening, true, 2, 4);

        // Student Order
        Order studentOrder = new Order(1, true, movieScreening);
        studentOrder.addSeatReservation(S_premiumTicketOne);
        studentOrder.addSeatReservation(S_premiumTicketTwo);
        studentOrder.addSeatReservation(S_normalTicketOne);
        studentOrder.addSeatReservation(S_normalTicketTwo);
        studentOrder.addSeatReservation(S_normalTicketThree);
        studentOrder.addSeatReservation(S_normalTicketFour);

        assertEquals(38.0, studentOrder.calculatePrice(),1e-15);
    }



    @Test
    public void NormalOrderTest(){

        // Movie and Screening
        Movie movie = new Movie("The A Team");
        MovieScreening movieScreening = new MovieScreening(movie, LocalDateTime.now(), 10);
        movie.addScreening(movieScreening);

        // Tickets for a normal order
        MovieTicket N_premiumTicketOne = new MovieTicket(movieScreening, false,  1, 1);
        MovieTicket N_premiumTicketTwo = new MovieTicket(movieScreening, false, 1, 2);
        MovieTicket N_normalTicketOne = new MovieTicket(movieScreening, false,  2, 1);
        MovieTicket N_normalTicketTwo = new MovieTicket(movieScreening, false,  2, 2);
        MovieTicket N_normalTicketThree = new MovieTicket(movieScreening, false,  2, 3);
        MovieTicket N_normalTicketFour = new MovieTicket(movieScreening, false, 2, 4);

        // Normal Order
        Order normalOrder = new Order(2, false, movieScreening);
        normalOrder.addSeatReservation(N_premiumTicketOne);
        normalOrder.addSeatReservation(N_premiumTicketTwo);
        normalOrder.addSeatReservation(N_normalTicketOne);
        normalOrder.addSeatReservation(N_normalTicketTwo);
        normalOrder.addSeatReservation(N_normalTicketThree);
        normalOrder.addSeatReservation(N_normalTicketFour);

        assertEquals(54.0, normalOrder.calculatePrice(),1e-15);

    }

    @Test
    public void NormalOrderMonday(){

        // Movie and Screening
        Movie movie = new Movie("The A Team");
        MovieScreening movieScreening = new MovieScreening(movie,  LocalDateTime.of(2020, 02, 3, 12, 12),  10);
        movie.addScreening(movieScreening);

        // Tickets for a normal order
        MovieTicket N_premiumTicketOne = new MovieTicket(movieScreening, false,  1, 1);
        MovieTicket N_premiumTicketTwo = new MovieTicket(movieScreening, false, 1, 2);
        MovieTicket N_normalTicketOne = new MovieTicket(movieScreening, false,  2, 1);
        MovieTicket N_normalTicketTwo = new MovieTicket(movieScreening, false,  2, 2);
        MovieTicket N_normalTicketThree = new MovieTicket(movieScreening, false,  2, 3);
        MovieTicket N_normalTicketFour = new MovieTicket(movieScreening, false, 2, 4);

        // Normal Order
        Order normalOrder = new Order(2, false, movieScreening);
        normalOrder.addSeatReservation(N_premiumTicketOne);
        normalOrder.addSeatReservation(N_premiumTicketTwo);
        normalOrder.addSeatReservation(N_normalTicketOne);
        normalOrder.addSeatReservation(N_normalTicketTwo);
        normalOrder.addSeatReservation(N_normalTicketThree);
        normalOrder.addSeatReservation(N_normalTicketFour);

        assertEquals(30.0, normalOrder.calculatePrice(),1e-15);

    }

    @Test
    public void NormalOrderTuesday(){

        // Movie and Screening
        Movie movie = new Movie("The A Team");
        MovieScreening movieScreening = new MovieScreening(movie,  LocalDateTime.of(2020, 02, 4, 12, 12),  10);
        movie.addScreening(movieScreening);

        // Tickets for a normal order
        MovieTicket N_premiumTicketOne = new MovieTicket(movieScreening, false,  1, 1);
        MovieTicket N_premiumTicketTwo = new MovieTicket(movieScreening, false, 1, 2);
        MovieTicket N_normalTicketOne = new MovieTicket(movieScreening, false,  2, 1);
        MovieTicket N_normalTicketTwo = new MovieTicket(movieScreening, false,  2, 2);
        MovieTicket N_normalTicketThree = new MovieTicket(movieScreening, false,  2, 3);
        MovieTicket N_normalTicketFour = new MovieTicket(movieScreening, false, 2, 4);

        // Normal Order
        Order normalOrder = new Order(2, false, movieScreening);
        normalOrder.addSeatReservation(N_premiumTicketOne);
        normalOrder.addSeatReservation(N_premiumTicketTwo);
        normalOrder.addSeatReservation(N_normalTicketOne);
        normalOrder.addSeatReservation(N_normalTicketTwo);
        normalOrder.addSeatReservation(N_normalTicketThree);
        normalOrder.addSeatReservation(N_normalTicketFour);

        assertEquals(30.0, normalOrder.calculatePrice(),1e-15);

    }

    @Test
    public void NormalOrderWednesday(){

        // Movie and Screening
        Movie movie = new Movie("The A Team");
        MovieScreening movieScreening = new MovieScreening(movie,  LocalDateTime.of(2020, 02, 5, 12, 12),  10);
        movie.addScreening(movieScreening);

        // Tickets for a normal order
        MovieTicket N_premiumTicketOne = new MovieTicket(movieScreening, false,  1, 1);
        MovieTicket N_premiumTicketTwo = new MovieTicket(movieScreening, false, 1, 2);
        MovieTicket N_normalTicketOne = new MovieTicket(movieScreening, false,  2, 1);
        MovieTicket N_normalTicketTwo = new MovieTicket(movieScreening, false,  2, 2);
        MovieTicket N_normalTicketThree = new MovieTicket(movieScreening, false,  2, 3);
        MovieTicket N_normalTicketFour = new MovieTicket(movieScreening, false, 2, 4);

        // Normal Order
        Order normalOrder = new Order(2, false, movieScreening);
        normalOrder.addSeatReservation(N_premiumTicketOne);
        normalOrder.addSeatReservation(N_premiumTicketTwo);
        normalOrder.addSeatReservation(N_normalTicketOne);
        normalOrder.addSeatReservation(N_normalTicketTwo);
        normalOrder.addSeatReservation(N_normalTicketThree);
        normalOrder.addSeatReservation(N_normalTicketFour);

        assertEquals(30.0, normalOrder.calculatePrice(),1e-15);

    }

    @Test
    public void NormalOrderThursday(){

        // Movie and Screening
        Movie movie = new Movie("The A Team");
        MovieScreening movieScreening = new MovieScreening(movie,  LocalDateTime.of(2020, 02, 6, 12, 12),  10);
        movie.addScreening(movieScreening);

        // Tickets for a normal order
        MovieTicket N_premiumTicketOne = new MovieTicket(movieScreening, false,  1, 1);
        MovieTicket N_premiumTicketTwo = new MovieTicket(movieScreening, false, 1, 2);
        MovieTicket N_normalTicketOne = new MovieTicket(movieScreening, false,  2, 1);
        MovieTicket N_normalTicketTwo = new MovieTicket(movieScreening, false,  2, 2);
        MovieTicket N_normalTicketThree = new MovieTicket(movieScreening, false,  2, 3);
        MovieTicket N_normalTicketFour = new MovieTicket(movieScreening, false, 2, 4);

        // Normal Order
        Order normalOrder = new Order(2, false, movieScreening);
        normalOrder.addSeatReservation(N_premiumTicketOne);
        normalOrder.addSeatReservation(N_premiumTicketTwo);
        normalOrder.addSeatReservation(N_normalTicketOne);
        normalOrder.addSeatReservation(N_normalTicketTwo);
        normalOrder.addSeatReservation(N_normalTicketThree);
        normalOrder.addSeatReservation(N_normalTicketFour);

        assertEquals(30.0, normalOrder.calculatePrice(),1e-15);

    }

    @Test
    public void NormalPremiumTicketOrder(){

        // Movie and Screening
        Movie movie = new Movie("The A Team");
        MovieScreening movieScreening = new MovieScreening(movie,  LocalDateTime.of(2020, 02, 6, 12, 12),  10);
        movie.addScreening(movieScreening);

        // Tickets for a normal order
        MovieTicket N_premiumTicketOne = new MovieTicket(movieScreening, true,  1, 1);
        MovieTicket N_premiumTicketTwo = new MovieTicket(movieScreening, true, 1, 2);
        MovieTicket N_normalTicketOne = new MovieTicket(movieScreening, true,  2, 1);
        MovieTicket N_normalTicketTwo = new MovieTicket(movieScreening, true,  2, 2);
        MovieTicket N_normalTicketThree = new MovieTicket(movieScreening, true,  2, 3);
        MovieTicket N_normalTicketFour = new MovieTicket(movieScreening, true, 2, 4);

        // Normal Order
        Order normalOrder = new Order(2, false, movieScreening);
        normalOrder.addSeatReservation(N_premiumTicketOne);
        normalOrder.addSeatReservation(N_premiumTicketTwo);
        normalOrder.addSeatReservation(N_normalTicketOne);
        normalOrder.addSeatReservation(N_normalTicketTwo);
        normalOrder.addSeatReservation(N_normalTicketThree);
        normalOrder.addSeatReservation(N_normalTicketFour);

        assertEquals(48.0, normalOrder.calculatePrice(),1e-15);

    }


    @Test
    public void MixedNormalTicketOrder(){

        // Movie and Screening
        Movie movie = new Movie("The A Team");
        MovieScreening movieScreening = new MovieScreening(movie,  LocalDateTime.of(2020, 02, 6, 12, 12),  10);
        movie.addScreening(movieScreening);

        // Tickets for a normal order
        MovieTicket N_premiumTicketOne = new MovieTicket(movieScreening, true,  1, 1);
        MovieTicket N_premiumTicketTwo = new MovieTicket(movieScreening, true, 1, 2);
        MovieTicket N_normalTicketOne = new MovieTicket(movieScreening, false,  2, 1);
        MovieTicket N_normalTicketTwo = new MovieTicket(movieScreening, false,  2, 2);
        MovieTicket N_normalTicketThree = new MovieTicket(movieScreening, true,  2, 3);
        MovieTicket N_normalTicketFour = new MovieTicket(movieScreening, true, 2, 4);

        // Normal Order
        Order normalOrder = new Order(2, false, movieScreening);
        normalOrder.addSeatReservation(N_premiumTicketOne);
        normalOrder.addSeatReservation(N_premiumTicketTwo);
        normalOrder.addSeatReservation(N_normalTicketOne);
        normalOrder.addSeatReservation(N_normalTicketTwo);
        normalOrder.addSeatReservation(N_normalTicketThree);
        normalOrder.addSeatReservation(N_normalTicketFour);

        assertEquals(42.0, normalOrder.calculatePrice(),1e-15);

    }

    @Test
    public void WeekendSevenNormalTicketOrder(){

        // Movie and Screening
        Movie movie = new Movie("The A Team");
        MovieScreening movieScreening = new MovieScreening(movie,  LocalDateTime.of(2020, 02, 8, 12, 12),  10);
        movie.addScreening(movieScreening);

        // Tickets for a normal order
        MovieTicket N_premiumTicketOne = new MovieTicket(movieScreening, false,  1, 1);
        MovieTicket N_premiumTicketTwo = new MovieTicket(movieScreening, false, 1, 2);
        MovieTicket N_normalTicketOne = new MovieTicket(movieScreening, false,  2, 1);
        MovieTicket N_normalTicketTwo = new MovieTicket(movieScreening, false,  2, 2);
        MovieTicket N_normalTicketThree = new MovieTicket(movieScreening, false,  2, 3);
        MovieTicket N_normalTicketFour = new MovieTicket(movieScreening, false, 2, 4);
        MovieTicket N_normalTicketFive = new MovieTicket(movieScreening, false, 2, 4);

        // Normal Order
        Order normalOrder = new Order(2, false, movieScreening);
        normalOrder.addSeatReservation(N_premiumTicketOne);
        normalOrder.addSeatReservation(N_premiumTicketTwo);
        normalOrder.addSeatReservation(N_normalTicketOne);
        normalOrder.addSeatReservation(N_normalTicketTwo);
        normalOrder.addSeatReservation(N_normalTicketThree);
        normalOrder.addSeatReservation(N_normalTicketFour);
        normalOrder.addSeatReservation(N_normalTicketFive);

        assertEquals(63.0, normalOrder.calculatePrice(),1e-15);

    }

    @Test
    public void WeekendTwelveNormalTicketOrder(){

        // Movie and Screening
        Movie movie = new Movie("The A Team");
        MovieScreening movieScreening = new MovieScreening(movie,  LocalDateTime.of(2020, 02, 8, 12, 12),  10);
        movie.addScreening(movieScreening);

        // Tickets for a normal order
        MovieTicket N_normalTicketOne = new MovieTicket(movieScreening, false,  2, 1);
        MovieTicket N_normalTicketTwo = new MovieTicket(movieScreening, false,  2, 2);
        MovieTicket N_normalTicketThree = new MovieTicket(movieScreening, false,  2, 3);
        MovieTicket N_normalTicketFour = new MovieTicket(movieScreening, false, 2, 4);
        MovieTicket N_normalTicketFive = new MovieTicket(movieScreening, false,  2, 1);
        MovieTicket N_normalTicketSix = new MovieTicket(movieScreening, false,  2, 2);
        MovieTicket N_normalTicketSeven = new MovieTicket(movieScreening, false,  2, 3);
        MovieTicket N_normalTicketEight = new MovieTicket(movieScreening, false, 2, 4);
        MovieTicket N_normalTicketNine = new MovieTicket(movieScreening, false, 2, 4);
        MovieTicket N_normalTicketTen = new MovieTicket(movieScreening, false, 2, 4);
        MovieTicket N_normalTicketEleven = new MovieTicket(movieScreening, false, 2, 4);
        MovieTicket N_normalTicketTwelve = new MovieTicket(movieScreening, false, 2, 4);

        // Normal Order
        Order normalOrder = new Order(2, false, movieScreening);
        normalOrder.addSeatReservation(N_normalTicketOne);
        normalOrder.addSeatReservation(N_normalTicketTwo);
        normalOrder.addSeatReservation(N_normalTicketThree);
        normalOrder.addSeatReservation(N_normalTicketFour);
        normalOrder.addSeatReservation(N_normalTicketFive);
        normalOrder.addSeatReservation(N_normalTicketSix);
        normalOrder.addSeatReservation(N_normalTicketSeven);
        normalOrder.addSeatReservation(N_normalTicketEight);
        normalOrder.addSeatReservation(N_normalTicketNine);
        normalOrder.addSeatReservation(N_normalTicketTen);
        normalOrder.addSeatReservation(N_normalTicketEleven);
        normalOrder.addSeatReservation(N_normalTicketTwelve);

        assertEquals(108.0, normalOrder.calculatePrice(),1e-15);
    }

    @Test
    public void WeekendFiveNormalTicketOrder(){

        // Movie and Screening
        Movie movie = new Movie("The A Team");
        MovieScreening movieScreening = new MovieScreening(movie,  LocalDateTime.of(2020, 02, 8, 12, 12),  10);
        movie.addScreening(movieScreening);

        // Tickets for a normal order
        MovieTicket N_normalTicketOne = new MovieTicket(movieScreening, false,  2, 1);
        MovieTicket N_normalTicketTwo = new MovieTicket(movieScreening, false,  2, 2);
        MovieTicket N_normalTicketThree = new MovieTicket(movieScreening, false,  2, 3);
        MovieTicket N_normalTicketFour = new MovieTicket(movieScreening, false, 2, 4);
        MovieTicket N_normalTicketFive = new MovieTicket(movieScreening, false,  2, 1);

        // Normal Order
        Order normalOrder = new Order(2, false, movieScreening);
        normalOrder.addSeatReservation(N_normalTicketOne);
        normalOrder.addSeatReservation(N_normalTicketTwo);
        normalOrder.addSeatReservation(N_normalTicketThree);
        normalOrder.addSeatReservation(N_normalTicketFour);
        normalOrder.addSeatReservation(N_normalTicketFive);
        assertEquals(50.0, normalOrder.calculatePrice(),1e-15);
    }
    @Test
    public void WeekendSixNormalTicketOrder(){

        // Movie and Screening
        Movie movie = new Movie("The A Team");
        MovieScreening movieScreening = new MovieScreening(movie,  LocalDateTime.of(2020, 02, 8, 12, 12),  10);
        movie.addScreening(movieScreening);

        // Tickets for a normal order
        MovieTicket N_normalTicketOne = new MovieTicket(movieScreening, false,  2, 1);
        MovieTicket N_normalTicketTwo = new MovieTicket(movieScreening, false,  2, 2);
        MovieTicket N_normalTicketThree = new MovieTicket(movieScreening, false,  2, 3);
        MovieTicket N_normalTicketFour = new MovieTicket(movieScreening, false, 2, 4);
        MovieTicket N_normalTicketFive = new MovieTicket(movieScreening, false,  2, 1);
        MovieTicket N_normalTicketSix = new MovieTicket(movieScreening, false,  2, 2);

        // Normal Order
        Order normalOrder = new Order(2, false, movieScreening);
        normalOrder.addSeatReservation(N_normalTicketOne);
        normalOrder.addSeatReservation(N_normalTicketTwo);
        normalOrder.addSeatReservation(N_normalTicketThree);
        normalOrder.addSeatReservation(N_normalTicketFour);
        normalOrder.addSeatReservation(N_normalTicketFive);
        normalOrder.addSeatReservation(N_normalTicketSix);

        assertEquals(54.0, normalOrder.calculatePrice(),1e-15);

    }

}