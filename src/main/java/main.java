import domain.*;

import java.io.IOException;
import java.time.LocalDateTime;

public class main {
    public static void main(String[] args) throws IOException {

        // Movie and Screening
        Movie movie = new Movie("The A Team");
        MovieScreening movieScreening = new MovieScreening(movie, LocalDateTime.now(), 10);
        movie.addScreening(movieScreening);


        // Tickets for a student order
        MovieTicket S_premiumTicketOne = new MovieTicket(movieScreening, true,  1, 1);
        MovieTicket S_premiumTicketTwo = new MovieTicket(movieScreening, true, 1, 2);
        MovieTicket S_normalTicketOne = new MovieTicket(movieScreening, false,  2, 1);
        MovieTicket S_normalTicketTwo = new MovieTicket(movieScreening, false,  2, 2);
        MovieTicket S_normalTicketThree = new MovieTicket(movieScreening, false,  2, 3);
        MovieTicket S_normalTicketFour = new MovieTicket(movieScreening, false, 2, 4);

        // Student Order
        Order studentOrder = new Order(1, true, movieScreening);
        studentOrder.addSeatReservation(S_premiumTicketOne);
        studentOrder.addSeatReservation(S_premiumTicketTwo);
        studentOrder.addSeatReservation(S_normalTicketOne);
        studentOrder.addSeatReservation(S_normalTicketTwo);
        studentOrder.addSeatReservation(S_normalTicketThree);
        studentOrder.addSeatReservation(S_normalTicketFour);


        // Tickets for a normal order
        MovieTicket N_premiumTicketOne = new MovieTicket(movieScreening, false,  1, 1);
        MovieTicket N_premiumTicketTwo = new MovieTicket(movieScreening, false, 1, 2);
        MovieTicket N_normalTicketOne = new MovieTicket(movieScreening, false,  2, 1);
        MovieTicket N_normalTicketTwo = new MovieTicket(movieScreening, false,  2, 2);
        MovieTicket N_normalTicketThree = new MovieTicket(movieScreening, true,  2, 3);
        MovieTicket N_normalTicketFour = new MovieTicket(movieScreening, false, 2, 4);

        // Normal Order
        Order normalOrder = new Order(2, false, movieScreening);
        normalOrder.addSeatReservation(N_premiumTicketOne);
        normalOrder.addSeatReservation(N_premiumTicketTwo);
        normalOrder.addSeatReservation(N_normalTicketOne);
        normalOrder.addSeatReservation(N_normalTicketTwo);
        normalOrder.addSeatReservation(N_normalTicketThree);
        normalOrder.addSeatReservation(N_normalTicketFour);

        System.out.println("Student total order price: " + studentOrder.calculatePrice());
        System.out.println("Normal total order price: " + normalOrder.calculatePrice());

        // Export
        studentOrder.export(TicketExportFormat.PLAINTEXT);
        studentOrder.export(TicketExportFormat.JSON);

        normalOrder.export((TicketExportFormat.PLAINTEXT));
        normalOrder.export(TicketExportFormat.JSON);
    }
}
