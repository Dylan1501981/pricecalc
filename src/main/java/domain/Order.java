package domain;

import org.json.simple.JSONObject;

import java.io.*;
import java.time.DayOfWeek;
import java.util.ArrayList;

public class Order
{
    private int orderNr;
    private boolean isStudentOrder;
    private MovieScreening movieScreening;
    private double total = 0.0;

    private ArrayList<MovieTicket> tickets;

    public Order(int orderNr, boolean isStudentOrder, MovieScreening movieScreening)
    {
        this.orderNr = orderNr;
        this.isStudentOrder = isStudentOrder;
        this.movieScreening = movieScreening;
        tickets = new ArrayList<MovieTicket>();
    }

    public int getOrderNr()
    {
        return orderNr;
    }

    public void addSeatReservation(MovieTicket ticket)
    {
        tickets.add(ticket);
    }

    public double calculatePrice()
    {
        if(isStudentOrder){
            int index = 0;
            for (MovieTicket ticket:tickets) {
                index++;
                if(ticket.isPremiumTicket()){
                    total = total + 2;
                }
                total = total + ticket.getPrice();

                if(index == 2){
                    total = total - ticket.getPrice();
                    index = 0;
                }
            }
        }else{
            DayOfWeek t = this.movieScreening.getDateAndTime().getDayOfWeek();
            if(t == DayOfWeek.MONDAY ||
                    t == DayOfWeek.TUESDAY ||
                    t == DayOfWeek.WEDNESDAY ||
                    t == DayOfWeek.THURSDAY){
                 int index = 0;
                 for (MovieTicket ticket:tickets) {
                     index++;

                     if(ticket.isPremiumTicket()){
                         total = total + 3;
                     }
                     total = total + ticket.getPrice();

                     if(index == 2){
                         total = total - ticket.getPrice();
                         index = 0;
                     }
                 }
             }else{

                for(MovieTicket ticket: tickets){
                    total = total + ticket.getPrice();
                }

               if(tickets.size() >= 6){
                   total = total * 0.9;
               }
            }
    }
    return total;
}

    public void export(TicketExportFormat exportFormat) throws IOException {
        if(exportFormat == TicketExportFormat.PLAINTEXT)
        {
            PrintWriter writer = null;
            try {
                writer = new PrintWriter("orders\\Order_" + getOrderNr(), "UTF-8");
                double totalWithoutDiscount = 0;

                for (MovieTicket ticket : tickets) {
                    totalWithoutDiscount += ticket.getPrice();
                    writer.println(ticket.toString());
                }
                writer.println("List price: " + totalWithoutDiscount);
                writer.println("Discount: " + (totalWithoutDiscount - total));
                writer.println("Total Order Cost:" + total);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } finally {
                writer.close();
            }
        }

        if(exportFormat == TicketExportFormat.JSON){
            JSONObject obj = new JSONObject();
            double totalWithoutDiscount = 0;

            for (MovieTicket ticket : tickets) {
                totalWithoutDiscount += ticket.getPrice();
                obj.put("Ticket", ticket.toString());
            }
            obj.put("List price", totalWithoutDiscount);
            obj.put("Discount", totalWithoutDiscount - total);
            obj.put("Cost", total);
            FileWriter file = new FileWriter("orders\\Order_" + getOrderNr() + ".txt");
            try{
                file.write(obj.toJSONString());

            }catch (IOException e){
                e.printStackTrace();
            }finally {
                file.flush();
                file.close();
            }
         }
        // Bases on the string respresentations of the tickets (toString), write
        // the ticket to a file with naming convention Order_<orderNr>.txt of
        // Order_<orderNr>.json
    }
}
